## Installation and setup

* Open a Linux terminal console on your laptop.

* Clone this git repository.
```
    $ git clone https://bitbucket.org/atis_elsts/mobility-experiments.git
```

* Update the serial port settings of your laptop:
```
     $ # need to do this as root
     $ sudo su
     # modprobe ftdi_sio
     # echo 0403 a6d1 > /sys/bus/usb-serial/drivers/ftdi_sio/new_id
     # exit
     $
```

* If you haven't done so, may need to add your user to `dialout` group, and then log out and log back in again (otherwise you need to use `sudo` to read from the serial port):
```
    $ sudo adduser atis dialout
```

* After this, you can connect SmartRF boards to your laptop and access them via serial port.

## Experiments

You wil be given a SmartRF board with a label. The label on the board is one of 192, 193, 194, 195.

There are number of experiments. 192 participates in all experiments.

193 participates in experiments:

    instant-2
    instant-3
    instant-4
    connections-2
    connections-3
    connections-4
    rpl-2
    rpl-3
    rpl-4

194 participates in experiments:

    instant-3
    instant-4
    connections-3
    connections-4
    rpl-3
    rpl-4

195 participates just in experiments:

    instant-4
    connections-4
    rpl-4

### Taking part in an experiment

For each experiment, a `make` target is provided. For example, to run `instant-4` experiment with node 195:

    atis@laptop:~/sphere/mobility-experiments$ make -f Makefile.195 instant-4

    make[1]: Entering directory '/home/atis/sphere/mobility-experiments/serial-io'
    make[1]: 'serialdump' is up to date.
    make[1]: Leaving directory '/home/atis/sphere/mobility-experiments/serial-io'
    cp: cannot stat 'instant-4-195.log': No such file or directory
    serial-io/serialdump -T%s /dev/ttyUSB1 | tee instant-4-195.log
    connecting to /dev/ttyUSB1 [OK]
    [1533138824] 
    [1533138824] TSCH: scanning on channel 25
    [1533138826] TSCH: scanning on channel 20

When this is done, follow the instructions to carry out the experiment. **The instructions will be provided on a printed paper sheet.**

Terminate the experiment with `Ctlr+C` (the resulting error message is nothing to worry about):

    ^CMakefile.195:14: recipe for target 'instant-4' failed
